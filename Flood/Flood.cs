using System;
using System.Collections.Generic;
using System.Text;

using Oxide.Core;
using Oxide.Core.Libraries.Covalence;

namespace Oxide.Plugins
{
    [Info("Flood", "nix01d", "1.1.0")]
    class Flood : CovalencePlugin
    {
        private const string FloodPermissions = "flood.perm";
        bool use;
        int waterlevel;
        Timer StormTimer;
        Timer ClearTimer;
        private void Init()
        {
            LoadConfig();
            permission.RegisterPermission(FloodPermissions, this);
        }

        [Command("storm")]
        private void cmdStorm(IPlayer player, string command, string[] args)
        {
            if(!player.HasPermission(FloodPermissions)) return;
            if (use == false)
                {
                    use = true;
                    player.Reply(covalence.FormatText(string.Format(lang.GetMessage("Потопище!!!1!1один!", this, player.Id), player.Name.Sanitize())));                    
                    server.Command("weather.load storm");
                    StormTimer = timer.Repeat(0.1f, 1000, () =>
                    {
                        waterlevel = waterlevel + 1;
                        server.Command("meta.add oceanlevel", 0.01);
                    });
                } else {
                    player.Reply("Давай, затопи здесь все ага..");
                }
        }  

        [Command("clear")]
        private void cmdClear(IPlayer player, string command, string[] args)
        {
            if(!player.HasPermission(FloodPermissions)) return;
            if (use == true)
                {
                    use = false;
                    timer.Destroy(ref StormTimer);
                    player.Reply(covalence.FormatText(string.Format(lang.GetMessage("Вроде утихло.", this, player.Id), player.Name.Sanitize())));
                    server.Command("weather.load clear");
                    ClearTimer = timer.Repeat(0.1f, waterlevel, () =>
                    {
                        server.Command("meta.add oceanlevel", -0.01);
                    });
                } else {
                    player.Reply("Уровень воды уже минимальный.");
                }

        }  
    }    
}
